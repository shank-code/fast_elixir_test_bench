# FastElixirTestBench

This repository contains benchmarks using [benchee](https://github.com/PragTob/benchee) for various code idioms in elixir. This repository is more of a personal playground for this kind of stuff. For a more authoritative source, checou the [fast-elixir](https://github.com/devonestes/fast-elixir) repository.

<!-- TODO: add links to the idioms -->

### `Enum.reverse()` vs `:lists.reverse()`

```
Operating System: Linux"
CPU Information: Intel(R) Core(TM) i3-4330 CPU @ 3.50GHz
Number of Available Cores: 4
Available memory: 4.84 GB
Elixir 1.8.0
Erlang 21.1.3

Benchmark suite executing with the following configuration:
warmup: 2 s
time: 10 s
memory time: 5 s
parallel: 1
inputs: Large (1 million), Medium (10 thousand), Small (1 thousand)
Estimated total run time: 1.70 min


Benchmarking :lists.reverse with input Large (1 million)...
Benchmarking :lists.reverse with input Medium (10 thousand)...
Benchmarking :lists.reverse with input Small (1 thousand)...
Warning: The function you are trying to benchmark is super fast, making measurements more unreliable!
This holds especially true for memory measurements.
See: https://github.com/PragTob/benchee/wiki/Benchee-Warnings#fast-execution-warning

You may disable this warning by passing print: [fast_warning: false] as configuration options.

Benchmarking Enum.reverse with input Large (1 million)...
Benchmarking Enum.reverse with input Medium (10 thousand)...
Benchmarking Enum.reverse with input Small (1 thousand)...
Warning: The function you are trying to benchmark is super fast, making measurements more unreliable!
This holds especially true for memory measurements.
See: https://github.com/PragTob/benchee/wiki/Benchee-Warnings#fast-execution-warning

You may disable this warning by passing print: [fast_warning: false] as configuration options.


##### With input Large (1 million) #####
Name                     ips        average  deviation         median         99th %
Enum.reverse           86.59       11.55 ms    ±30.75%       10.68 ms       22.80 ms
:lists.reverse         85.22       11.73 ms    ±29.91%       10.49 ms       22.31 ms

Comparison: 
Enum.reverse           86.59
:lists.reverse         85.22 - 1.02x slower

Memory usage statistics:

Name              Memory usage
Enum.reverse          25.64 MB
:lists.reverse        25.64 MB - 1.00x memory usage

**All measurements for memory usage were the same**

##### With input Medium (10 thousand) #####
Name                     ips        average  deviation         median         99th %
Enum.reverse         19.89 K       50.29 μs   ±115.41%          37 μs         253 μs
:lists.reverse       18.85 K       53.05 μs   ±131.08%          38 μs         271 μs

Comparison: 
Enum.reverse         19.89 K
:lists.reverse       18.85 K - 1.05x slower

Memory usage statistics:

Name              Memory usage
Enum.reverse         224.04 KB
:lists.reverse       224.04 KB - 1.00x memory usage

**All measurements for memory usage were the same**

##### With input Small (1 thousand) #####
Name                     ips        average  deviation         median         99th %
Enum.reverse          1.19 M        0.84 μs   ±797.81%        0.60 μs        4.70 μs
:lists.reverse        1.13 M        0.89 μs   ±723.78%        0.60 μs        4.70 μs

Comparison: 
Enum.reverse          1.19 M
:lists.reverse        1.13 M - 1.06x slower

Memory usage statistics:

Name              Memory usage
Enum.reverse           2.84 KB
:lists.reverse         2.84 KB - 1.00x memory usage

**All measurements for memory usage were the same**
```