inputs = %{
  "Small (1 thousand)" => Enum.to_list(1..100),
  "Medium (10 thousand)" => Enum.to_list(1..10_000),
  "Large (1 million)" => Enum.to_list(1..1_000_000)
}

Benchee.run(
  %{
    "Enum.reverse" => fn list -> Enum.reverse(list) end,
    ":lists.reverse" => fn list -> :lists.reverse(list) end
  },
  time: 10,
  inputs: inputs,
  memory_time: 5
)
